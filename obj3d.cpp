#include "obj3d.h"



obj3d::obj3d(string objPath)
{
	this->path = objPath;
}


obj3d::~obj3d()
{
}

void obj3d::loadModel()
{
	if (loadOBJ(this->path.c_str(), verticesSpace, uvsSpace, normalsSpace)) {


		//GLuint VertexArrayIDSpaceship; //GLuint arrayCube
		glGenVertexArrays(1, &vertexArrayID); //Array anlegen
		glBindVertexArray(vertexArrayID); //Binde

		// Ein ArrayBuffer speichert Daten zu Eckpunkten (hier xyz bzw. Position)
		//GLuint vertexbuffer2;
		glGenBuffers(1, &vertexbufferID); // Kennung erhalten anlegen des Buffers mit id
		glBindBuffer(GL_ARRAY_BUFFER, vertexbufferID); // Daten zur Kennung definieren
													  // Buffer zugreifbar f�r die Shader machen
		glBufferData(GL_ARRAY_BUFFER, verticesSpace.size() * sizeof(glm::vec3), &verticesSpace[0], GL_STATIC_DRAW); // aus dem LoadOBJ


																													// Zeilen "v" aus cube.obj
																													// Erst nach glEnableVertexAttribArray kann DrawArrays auf die Daten zugreifen...
		glEnableVertexAttribArray(0); // siehe layout im vertex shader: location = 0 / Kanal auf zum Shader
		glVertexAttribPointer(0,  // location = 0 / Wie die Daten "kommen" Protokol
			3,  // Datenformat vec3: 3 floats fuer xyz 
			GL_FLOAT,
			GL_FALSE, // Fixedpoint data normalisieren ?
			0, // Eckpunkte direkt hintereinander gespeichert
			(void*)0); // abweichender Datenanfang ? 


		//Zeilen "vn" cube.obj
		//GLuint normalbuffer2; // Hier alles analog f�r Normalen in location == 2
		glGenBuffers(1, &normalbuffer);
		glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
		glBufferData(GL_ARRAY_BUFFER, normalsSpace.size() * sizeof(glm::vec3), &normalsSpace[0], GL_STATIC_DRAW);
		glEnableVertexAttribArray(2); // siehe layout im vertex shader 
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

		//Zeilen "vt" cube.obj
		//GLuint uvbuffer2; // Hier alles analog f�r Texturkoordinaten in location == 1 (2 floats u und v!)
		glGenBuffers(1, &uvbufferID);
		glBindBuffer(GL_ARRAY_BUFFER, uvbufferID);
		glBufferData(GL_ARRAY_BUFFER, uvsSpace.size() * sizeof(glm::vec2), &uvsSpace[0], GL_STATIC_DRAW);
		glEnableVertexAttribArray(1); // siehe layout im vertex shader 
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
		
	}

}

GLuint obj3d::getVertexArrayID()
{
	return vertexArrayID;
}

GLuint obj3d::getVertexbufferID()
{
	return vertexbufferID;
}

GLuint obj3d::getNormalbuffer()
{
	return normalbuffer;
}

GLuint obj3d::getUvbufferID()
{
	return uvbufferID;
}

vector<glm::vec3> obj3d::getVertices()
{
	return verticesSpace;
}

vector<glm::vec2> obj3d::getUvs()
{
	return uvsSpace;
}

vector<glm::vec3> obj3d::getNormals()
{
	return normalsSpace;
}

void obj3d::clear()
{
	// Cleanup VBO and shader
	glDeleteBuffers(1, &vertexbufferID);
	glDeleteBuffers(1, &normalbuffer);

	glDeleteBuffers(1, &uvbufferID);
}
