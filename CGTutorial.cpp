﻿// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include "stb_image.h"

#include <chrono>
#include <thread>

// Include f�r Random
#include <random>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace glm;

// Achtung, die OpenGL-Tutorials nutzen glfw 2.7, glfw kommt mit einem veränderten API schon in der Version 3 

// Befindet sich bei den OpenGL-Tutorials unter "common"
#include "shader.hpp"

// Wuerfel und Kugel
#include "objects.hpp"

// Ab Uebung5 werden objloader.hpp und cpp benoetigt
#include "objloader.hpp"

//Texture lesen
#include "texture.hpp"

#include "obj3d.h"

//Für Highscore rendering
#include "text2D.hpp"




// Diese Drei Matrizen global (Singleton-Muster), damit sie jederzeit modifiziert und
// an die Grafikkarte geschickt werden koennen
glm::mat4 Projection;
glm::mat4 View;
glm::mat4 Model;
glm::mat4 Save;

glm::vec3 cameraPos;

GLuint programID;

glm::mat4 modelplayer, modelobstacle, modelobstacle_left, modelobstacle_right;

//In klasse auslagern und erben von Object
int const OBJECT_CUBE = 0;
int const OBJECT_REPAIR = 1;
int const OBJECT_ENEMY_SHIP = 2;
int const OBJECT_PLAYER = 3;
int const OBJECT_STAR = 4;

const float OBSTACLE_POSITION_LEFT = -7;
const float OBSTACLE_POSITION_RIGHT = 7;
const float OBSTACLE_POSITION_MIDDLE = 0;

const float SPAWN_DISTANCE = 300;

// Spielrelevante Variablen initialisieren
bool paused = false;
float highscore = 0;
float deltaTime = 0;
float oldTime = 0;
float newTime = 0;
float lastSpawn = 0;
float rotationCamera = 0.0f;

struct aabb {
	glm::vec3 m_max;
	glm::vec3 m_min;
};

struct Player {
	float x, y, z;
	int maxLifes = 4;
	int lifes = maxLifes;
	float velocity = 50;
	float MAX_PLAYER_VELOCITY = 450;
	float acceleration = 1.1f;
	float timeOnSpeed;
	aabb boundingBox;
};

struct Object {
	int type;
	float x, y, z;
	aabb boundingBox;
	bool hit = false;
};

// Spieler und Powerups
Player player;


//ARRAY mit aktuellen Hindernissen auf dem "Feld"
vector<Object> obstaclesOnField;

bool checkCollision(const aabb &p, const aabb &obstacle);
void drawObstacles();
void spawnObstacles(float lane);
void spawnObstaclePerLane();
void moveObstacles();
void reset();
void pause(bool willBePaused);
void sendMVP();
int generateRandomObstacleType();
void rotateCamera(float deltaTime);

// Skybox
GLuint make_big_cube();
bool load_cube_map_side(
	GLuint texture, GLenum side_target, const char* file_name);
void create_cube_map(
	const char* front,
	const char* back,
	const char* top,
	const char* bottom,
	const char* left,
	const char* right,
	GLuint* tex_cube);

aabb getminmax(/*glm::mat4 &model*/vector<glm::vec3> verticesSpace);
aabb translateBoundingBox(glm::mat4 model, int obstacleTyp);
void drawPlayer();

aabb playerCol, cubeCol, space_shipCol, repairCol, starCol;

// Alle eingeladenen Objekte
obj3d *objectRepair;
obj3d *objectSpaceship;
obj3d *objectCube_test;
obj3d *objectStar;


// Textures
GLuint shipMetalTexture = 0;
GLuint FontTexture = 0;
GLuint bombTexture = 0;
GLuint starTexture = 0;

void error_callback(int error, const char* description);



//Kamera Interaktion und Steuerung statt rotation translation

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	
	if(!paused){
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			if (player.x < OBSTACLE_POSITION_RIGHT)
				player.x += 7;

		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			if (player.x > OBSTACLE_POSITION_LEFT)
				player.x -= 7;
	}

	if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS && player.lifes > 0)
		pause(!paused);

	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
		reset();



	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
	
}


int main(void)
{
	// Initialise GLFW
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		exit(EXIT_FAILURE);
	}

	// Fehler werden auf stderr ausgegeben, s. o.
	glfwSetErrorCallback(error_callback);

	// Open a window and create its OpenGL context
	// glfwWindowHint vorher aufrufen, um erforderliche Resourcen festzulegen
	GLFWwindow* window = glfwCreateWindow(1280, // Breite
										  720,  // Hoehe
										  "CG - SpaceRacer", // Ueberschrift
										  NULL,  // windowed mode
										  NULL); // shared windoe

	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	// Make the window's context current (wird nicht automatisch gemacht)
    glfwMakeContextCurrent(window);

	// Initialize GLEW
	// GLEW ermöglicht Zugriff auf OpenGL-API > 1.1
	glewExperimental = true; // Needed for core profile

	if (glewInit() != GLEW_OK)
	{
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	// Auf Keyboard-Events reagieren
	glfwSetKeyCallback(window, key_callback);

	// Textures laden
	shipMetalTexture = loadBMP_custom("Metal_texture.bmp");
	bombTexture = loadBMP_custom("mandrill.bmp");
	starTexture = loadBMP_custom("yellow.bmp");


	//Model laden

	objectRepair = new obj3d("wrench1.obj");
	objectRepair->loadModel();
	repairCol = getminmax(objectRepair->getVertices());


	objectSpaceship = new obj3d("spaceship.obj");
	objectSpaceship->loadModel();
	space_shipCol = getminmax(objectSpaceship->getVertices());
	playerCol = getminmax(objectSpaceship->getVertices());

	objectStar = new obj3d("star.obj");
	objectStar->loadModel();
	starCol = getminmax(objectStar->getVertices());

	objectCube_test = new obj3d("cube_test.obj");
	objectCube_test->loadModel();
	cubeCol = getminmax(objectCube_test->getVertices());

	// Rendering highscore and player lifes
	// Load the texture
	FontTexture = loadDDS("uvmap.DDS");
	// Initialize our little text library with the Holstein font
	initText2D("Holstein.DDS");//http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-11-2d-text/

							   /*---------------------------------SKYBOX CUBE MAP-----------------------------------*/
	GLuint cube_vao = make_big_cube();
	GLuint cube_map_texture = 0;
	create_cube_map("front.bmp", "back.bmp", "top.bmp", "bottom.bmp", "right.bmp", "left.bmp", &cube_map_texture); // Textures from: https://www.dropbox.com/sh/bglrwv6p6o4p7sn/AABvudVRx7OMjnpY-vuqz2gsa
																												   // cube-map shaders
	GLuint cube_sp = LoadShaders("skybox.vs", "skybox.frag");


	//Aktivierung von Z Buffer bzw TiefenTest
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	//Shader ID festlegen
	// Create and compile our GLSL program from the shaders
	//programID = LoadShaders("TransformVertexShader.vertexshader","ColorFragmentShader.fragmentshader");//"StandardShading.vertexshader", "StandardShading.fragmentshader");
	programID = LoadShaders("StandardShading.vertexshader", "StandardShading.fragmentshader");

	// Shader auch benutzen ! indem man die ProgID Uebergibt
	glUseProgram(programID);

	// Beleuchtung
	glm::vec3 lightPos = glm::vec3(0, 4, -5);
	glUniform3f(glGetUniformLocation(programID, "LightPosition_worldspace"), lightPos.x, lightPos.y, lightPos.z);
	
	// Um Ladedauer am Anfang zu ignorieren
	glfwSetTime(0);

	// Eventloop
	while (!glfwWindowShouldClose(window))
	{
		glUseProgram(programID);

		oldTime = newTime;
		newTime = (float) glfwGetTime();

		deltaTime = newTime - oldTime;

		player.timeOnSpeed += deltaTime;

		if (player.timeOnSpeed >= 3 && player.velocity < player.MAX_PLAYER_VELOCITY) {
			player.velocity *= player.acceleration;
			if (player.velocity > player.MAX_PLAYER_VELOCITY)
				player.velocity = player.MAX_PLAYER_VELOCITY;
			player.timeOnSpeed = 0;
		}

		// Clear the screen und Z-Buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	
		// Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
		Projection = glm::perspective(45.0f, 1280.0f / 720.0f, 0.01f, 1000.0f);
		// Camera matrix
		View = glm::lookAt(glm::vec3(0,1.5, -4), // Camera is at (0,1.5,-4), in World Space
						   glm::vec3(0,1.0,0),  // and looks at x=0, y=1, z=0
						   glm::vec3(0,1,0)); // Head is up (set to 0,1,0 to look upside-down) rotation ( auf dem Kopf usw)

		if(paused) {
			rotateCamera(deltaTime);
		}
		// Model matrix : an identity matrix (model will be at the origin) / was passiert mit denn objekten
		Model = glm::mat4(1.0f);
		// Sichere Stand um dann sachen nur fuer ein Objekt gelten
		Save = Model;

		// note that this view matrix should NOT contain camera translation.
		int cube_V_location = glGetUniformLocation(cube_sp, "V");
		int cube_P_location = glGetUniformLocation(cube_sp, "P");
		glUseProgram(cube_sp);

		glUniformMatrix4fv(cube_V_location, 1, GL_FALSE, glm::value_ptr(View));
		glUniformMatrix4fv(cube_P_location, 1, GL_FALSE, glm::value_ptr(Projection));

		// render a sky-box using the cube-map texture
		glDepthMask(GL_FALSE);
		glUseProgram(cube_sp);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, cube_map_texture);
		glBindVertexArray(cube_vao);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		glDepthMask(GL_TRUE);

		glUseProgram(programID);

		drawPlayer();

		if(!paused){
			if (player.lifes <= 0) {
				pause(true);
			}
			spawnObstaclePerLane();
			moveObstacles();

		}

		drawObstacles();

		// Text here
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, FontTexture);
		glUniform1i(glGetUniformLocation(programID, "myTextureSampler"), 0);
		char text[256];
		char plifes[256];
		sprintf(text, "SCORE: %u", static_cast<int>(highscore));
		sprintf(plifes, "HEALTH: %d", player.lifes);
		// chararray, x pos, y pos, font size
		printText2D(text, 10, 570, 20);
		printText2D(plifes, 10, 530, 20);

		if (player.lifes <= 0) {


			// Text here
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, FontTexture);
			glUniform1i(glGetUniformLocation(programID, "myTextureSampler"), 0);
			char gameOver[256];
			char pressR[256];
			sprintf(gameOver, "GAME OVER");
			sprintf(pressR, "PRESS R TO RESTART");
			// chararray, x pos, y pos, font size
			printText2D(gameOver, 180, 300, 50);
			printText2D(pressR, 182, 250, 25);

		}
		else if (paused) {
			player.timeOnSpeed = 0;

			// Text here
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, FontTexture);
			glUniform1i(glGetUniformLocation(programID, "myTextureSampler"), 0);
			char paused[256];
			sprintf(paused, "PAUSED");
			// chararray, x pos, y pos, font size
			printText2D(paused, 250, 300, 50);
		}
		// Swap buffers
		glfwSwapBuffers(window);

		// Poll for and process events 
        glfwPollEvents();
	} 


	glDeleteProgram(programID);
	cleanupText2D();
	//Leert Buffer Normalbuffer uvBuffer
	objectSpaceship->clear();
	objectRepair->clear();
	objectCube_test->clear();
	
	glDeleteTextures(1, &shipMetalTexture);
	glDeleteTextures(1, &bombTexture);


	// Close OpenGL window and terminate GLFW
	glfwTerminate();
	return 0;
}

void sendMVP()
{
	// Our ModelViewProjection : multiplication of our 3 matrices
	glm::mat4 MVP = Projection * View * Model;
	// Send our transformation to the currently bound shader, 
	// in the "MVP" uniform, konstant fuer alle Eckpunkte
	glUniformMatrix4fv(glGetUniformLocation(programID, "MVP"), 1, GL_FALSE, &MVP[0][0]);

	//NEU
	glUniformMatrix4fv(glGetUniformLocation(programID, "M"), 1, GL_FALSE, &Model[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(programID, "V"), 1, GL_FALSE, &View[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(programID, "P"), 1, GL_FALSE, &Projection[0][0]);

}

//Bewegt Objeckte
void moveObstacles() {

	
	for (int i = 0; i < obstaclesOnField.size(); i++) {

		if (obstaclesOnField[i].x == player.x) {
			if (checkCollision(obstaclesOnField[i].boundingBox, player.boundingBox)) {
				if (obstaclesOnField[i].type == OBJECT_REPAIR && player.lifes<player.maxLifes) {
					player.lifes++;
					cout << "life added: " << player.lifes << endl;
				}
				else if (obstaclesOnField[i].type == OBJECT_STAR) {
					cout << "BONUS POINTS!" << endl;
					highscore += 1000;
				}
				else if (obstaclesOnField[i].type != OBJECT_REPAIR && !obstaclesOnField[i].hit) {
					player.lifes--;
					cout << "life lost: " << player.lifes << endl;
				}

				obstaclesOnField[i].hit = true;
			}
		}

		if (obstaclesOnField[i].z < -10 || obstaclesOnField[i].hit) {
			swap(obstaclesOnField[i], obstaclesOnField.back());
			obstaclesOnField.pop_back();
		}
		else {
			obstaclesOnField[i].z -= player.velocity *deltaTime;
		}
	}

	highscore += player.velocity*deltaTime;
}

void drawPlayer() {

	Save = Model;
	//Zeichne Raumschiff
	Model = glm::scale(Model, glm::vec3(1.0 / 5.0, 1.0 / 5.0, 1.0 / 5.0));
	//@@@@@@@@@@@@@@@@@@@@@@@@ Ship nach rechts und links bewegen
	Model = glm::translate(Model, glm::vec3(player.x, player.y, player.z));

	player.boundingBox = translateBoundingBox(Model, OBJECT_PLAYER);

	//zur graka schicken
	sendMVP();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, shipMetalTexture);
	glUniform1i(glGetUniformLocation(programID, "myTextureSampler"), 0);
	// "Object" zeichnen mit neuem Objekt
	glBindVertexArray(objectSpaceship->getVertexArrayID());
	glDrawArrays(GL_TRIANGLES, 0, objectSpaceship->getVertices().size());
	glBindVertexArray(0);

	Model = Save;

}

// Zeichnet Obstacles
void drawObstacles(){

	std::vector<Object>::iterator objectIterator;
	for (objectIterator = obstaclesOnField.begin(); objectIterator != obstaclesOnField.end(); ++objectIterator) {

		switch (objectIterator->type) {

			case OBJECT_STAR:
			{
				Save = Model;
				//rausnehmen
				Model = glm::scale(Model, glm::vec3(1.0 / 5.0, 1.0 / 5.0, 1.0 / 5.0));
				Model = glm::translate(Model, glm::vec3(objectIterator->x, objectIterator->y, objectIterator->z));

				objectIterator->boundingBox = translateBoundingBox(Model, OBJECT_STAR);
				sendMVP();

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, starTexture);
				glUniform1i(glGetUniformLocation(programID, "myTextureSampler"), 0);
				// "Object" zeichnen mit neuem Objekt
				glBindVertexArray(objectStar->getVertexArrayID());
				glDrawArrays(GL_TRIANGLES, 0, objectStar->getVertices().size());
				glBindVertexArray(0);

				Model = Save;
				break;
			}
			case OBJECT_CUBE:
			{
				Save = Model;
				//rausnehmen
				Model = glm::scale(Model, glm::vec3(1.0 / 5.0, 1.0 / 5.0, 1.0 / 5.0));
				Model = glm::translate(Model, glm::vec3(objectIterator->x, objectIterator->y, objectIterator->z));

				objectIterator->boundingBox = translateBoundingBox(Model, OBJECT_CUBE);
				sendMVP();

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, shipMetalTexture);
				glUniform1i(glGetUniformLocation(programID, "myTextureSampler"), 0);
				// "Object" zeichnen mit neuem Objekt
				glBindVertexArray(objectCube_test->getVertexArrayID());
				glDrawArrays(GL_TRIANGLES, 0, objectCube_test->getVertices().size());
				glBindVertexArray(0);

				Model = Save;
				break;
			}
			case OBJECT_REPAIR:
			{
				Save = Model;
				//rausnehmen
				Model = glm::scale(Model, glm::vec3(1.0 / 5.0, 1.0 / 5.0, 1.0 / 5.0));
				Model = glm::translate(Model, glm::vec3(objectIterator->x, objectIterator->y, objectIterator->z));

				objectIterator->boundingBox = translateBoundingBox(Model, OBJECT_REPAIR);

				sendMVP();
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, shipMetalTexture);
				glUniform1i(glGetUniformLocation(programID, "myTextureSampler"), 0);
				// "Object" zeichnen mit neuem Objekt
				glBindVertexArray(objectRepair->getVertexArrayID());
				glDrawArrays(GL_TRIANGLES, 0, objectRepair->getVertices().size());
				glBindVertexArray(0);

				Model = Save;
				break;
			}
			case OBJECT_ENEMY_SHIP:
			{
				Save = Model;
				//rausnehmen
				Model = glm::scale(Model, glm::vec3(1.0 / 5.0, 1.0 / 5.0, 1.0 / 5.0));
				Model = glm::translate(Model, glm::vec3(objectIterator->x, objectIterator->y, objectIterator->z));
				//Model = glm::rotate(Model, (glm::mediump_float)180, glm::vec3(0, 1, 0));
				objectIterator->boundingBox = translateBoundingBox(Model, OBJECT_ENEMY_SHIP);

				sendMVP();
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, shipMetalTexture);
				glUniform1i(glGetUniformLocation(programID, "myTextureSampler"), 0);
				// "Object" zeichnen mit neuem Objekt
				glBindVertexArray(objectSpaceship->getVertexArrayID());
				glDrawArrays(GL_TRIANGLES, 0, objectSpaceship->getVertices().size());
				glBindVertexArray(0);

				Model = Save;

				break;
			}
		}	
	}
}

//Ort des Hindernisses zuf�llig hinzuf�gen
void spawnObstaclePerLane()
{
	std::random_device rd;
	std::mt19937 rng(rd());
	std::uniform_int_distribution<int> uni(0, 2); // CODE VON: http://stackoverflow.com/questions/5008804/generating-random-integer-from-a-range zur Implementierung
													// Einer Zufallszahl
	int random_int = uni(rng);
	
	float lanes[] = { OBSTACLE_POSITION_LEFT , OBSTACLE_POSITION_MIDDLE, OBSTACLE_POSITION_RIGHT };

	float lastObstacleDistance = glfwGetTime() - lastSpawn;

	float minimumSpawnDistance = 80;

	if (lastObstacleDistance*player.velocity > player.velocity || lastObstacleDistance*player.velocity > minimumSpawnDistance){
		spawnObstacles(lanes[random_int]);
		lastSpawn = glfwGetTime();
	}
}

//generiert ein zufälliges obstacle Objekt
int generateRandomObstacleType() {

	std::random_device rd;
	std::mt19937 rng(rd());
	std::uniform_int_distribution<int> uni(0, 54); // CODE VON: http://stackoverflow.com/questions/5008804/generating-random-integer-from-a-range zur Implementierung
												  // Einer Zufallszahl
	int random_int = uni(rng);


	int spawnChances[] = { 20, 5, 30 }; // cube, repair, enemyShip
	

	if (random_int > 0 && random_int < spawnChances[0]){
		//cout << "SpawnInteger: " << random_int << " entspricht ObstacleID: " << OBJECT_CUBE << endl;
		return OBJECT_STAR;
	}
	else if (random_int >= spawnChances[0] && random_int < spawnChances[0] + spawnChances[1]) {
		//cout << "SpawnInteger: " << random_int << " entspricht ObstacleID: " << OBJECT_REPAIR << endl;
		return OBJECT_REPAIR;
	}
	else if (random_int >= spawnChances[0] + spawnChances[1] && random_int < spawnChances[0] + spawnChances[1] + spawnChances[2]) {
		//cout << "SpawnInteger: " << random_int << " entspricht ObstacleID: " << OBJECT_ENEMY_SHIP << endl;
		return OBJECT_ENEMY_SHIP;
	}
	else if (random_int >= 55)
		return 3;
}

// Zeichnet ein bestimmtes obstacle Objekt auf eine bestimmte position 
void spawnObstacles(float lane) {
	
	int obstacleID = generateRandomObstacleType();	

		switch (obstacleID) {
		case OBJECT_STAR:
		{
			Object star;
			star.x = lane;
			star.y = 0; //Konstante?
			star.z = SPAWN_DISTANCE; //Standard entfernung bzw auch konstante?
			star.type = OBJECT_STAR;
			obstaclesOnField.push_back(star);
		}
			break;
		case OBJECT_REPAIR:
		{
			Object repair;
			repair.x = lane;
			repair.y = 0; //Konstante?
			repair.z = SPAWN_DISTANCE; //Standard entfernung bzw auch konstante?
			repair.type = OBJECT_REPAIR;
			obstaclesOnField.push_back(repair);
		}
			break;
		case OBJECT_ENEMY_SHIP:
		{
			Object enemy_ship;
			enemy_ship.x = lane;
			enemy_ship.y = 0; //Konstante?
			enemy_ship.z = SPAWN_DISTANCE; //Standard entfernung bzw auch konstante?
			enemy_ship.type = OBJECT_ENEMY_SHIP;
			obstaclesOnField.push_back(enemy_ship);
		}
			break;
		default: break;

	}
}

bool checkCollision(const aabb &p, const aabb &obstacle)
{
	//ObjectonField array durchlaufen und mit unserem rausschiff checken

	return(p.m_max.x > obstacle.m_min.x&&
		p.m_min.x < obstacle.m_max.x&&
		p.m_max.y > obstacle.m_min.y &&
		p.m_min.y < obstacle.m_max.y &&
		p.m_max.z > obstacle.m_min.z &&
		p.m_min.z < obstacle.m_max.z);
}

aabb getminmax(vector<glm::vec3> verticesSpace)
{
	glm::vec3 max = verticesSpace[0];
	glm::vec3 min = verticesSpace[0];

	//hier wird durch die ganzen Punkte des o.g. W�rfels iteriert um die min. und max. Koordinaten zu erhalten
	for (int x = 0; x < 36; x++) {
		//cube[x] = model * cube[x];
		if (verticesSpace[x].x < min.x)
			min.x = verticesSpace[x].x;
		if (verticesSpace[x].y < min.y)
			min.y = verticesSpace[x].y;
		if (verticesSpace[x].z < min.z)
			min.z = verticesSpace[x].z;
		if (verticesSpace[x].x > max.x)
			max.x = verticesSpace[x].x;
		if (verticesSpace[x].y > max.y)
			max.y = verticesSpace[x].y;
		if (verticesSpace[x].z > max.z)
			max.z = verticesSpace[x].z;

	}
	//ergebnis ist ein struct mit den min. max. Werten des fiktiven W�rfels, der den jeweiligen K�rper umh�llt.
	aabb result{ glm::vec3{ max.x,max.y,max.z } ,glm::vec3{ min.x,min.y,min.z } };
	return result;
}

void error_callback(int error, const char* description)
{
	fputs(description, stderr);
}

void pause(bool willBePaused) {
	paused = willBePaused;
	rotationCamera = 0;
}

aabb translateBoundingBox(glm::mat4 model, int obstacleTyp) {

	aabb newBoundingBox;
	glm::vec4 tmpMin;
	glm::vec4 tmpMax;
	glm::vec3 newMin;
	glm::vec3 newMax;

	switch (obstacleTyp) {

	case OBJECT_STAR:

		tmpMin = glm::vec4(starCol.m_min.x, starCol.m_min.y, starCol.m_min.z, 1);
		tmpMax = glm::vec4(starCol.m_max.x, starCol.m_max.y, starCol.m_max.z, 1);
		tmpMin = model * tmpMin;
		tmpMax = model * tmpMax;

		//https://stackoverflow.com/questions/14657303/convert-glmvec4-to-glmvec3
		newMin = glm::vec3(tmpMin);
		newMax = glm::vec3(tmpMax);

		newBoundingBox.m_min = newMin;
		newBoundingBox.m_max = newMax;

		return newBoundingBox;
		break;

	case OBJECT_CUBE:

		tmpMin = glm::vec4(cubeCol.m_min.x, cubeCol.m_min.y, cubeCol.m_min.z, 1);
		tmpMax = glm::vec4(cubeCol.m_max.x, cubeCol.m_max.y, cubeCol.m_max.z, 1);
		tmpMin = model * tmpMin;
		tmpMax = model * tmpMax;

		//https://stackoverflow.com/questions/14657303/convert-glmvec4-to-glmvec3
		newMin = glm::vec3 (tmpMin);
		newMax = glm::vec3 (tmpMax);

		newBoundingBox.m_min = newMin;
		newBoundingBox.m_max = newMax;

		return newBoundingBox;
		break;

	case OBJECT_REPAIR:

		tmpMin = glm::vec4 (repairCol.m_min.x, repairCol.m_min.y, repairCol.m_min.z, 1);
		tmpMax = glm::vec4 (repairCol.m_max.x, repairCol.m_max.y, repairCol.m_max.z, 1);
		tmpMin = model * tmpMin;
		tmpMax = model * tmpMax;

		//https://stackoverflow.com/questions/14657303/convert-glmvec4-to-glmvec3
		newMin = glm::vec3(tmpMin);
		newMax = glm::vec3(tmpMax);

		newBoundingBox.m_min = newMin;
		newBoundingBox.m_max = newMax;

		return newBoundingBox;
		break;

	case OBJECT_ENEMY_SHIP:

		tmpMin = glm::vec4 (space_shipCol.m_min.x, space_shipCol.m_min.y, space_shipCol.m_min.z, 1);
		tmpMax = glm::vec4 (space_shipCol.m_max.x, space_shipCol.m_max.y, space_shipCol.m_max.z, 1);
		tmpMin = model * tmpMin;
		tmpMax = model * tmpMax;

		//https://stackoverflow.com/questions/14657303/convert-glmvec4-to-glmvec3
		newMin = glm::vec3 (tmpMin);
		newMax = glm::vec3 (tmpMax);

		newBoundingBox.m_min = newMin;
		newBoundingBox.m_max = newMax;

		return newBoundingBox;
		break;

	case OBJECT_PLAYER:

		tmpMin = glm::vec4(playerCol.m_min.x, playerCol.m_min.y, playerCol.m_min.z, 1);
		tmpMax = glm::vec4(playerCol.m_max.x, playerCol.m_max.y, playerCol.m_max.z, 1);
		tmpMin = model * tmpMin;
		tmpMax = model * tmpMax;

		//https://stackoverflow.com/questions/14657303/convert-glmvec4-to-glmvec3
		newMin = glm::vec3(tmpMin);
		newMax = glm::vec3(tmpMax);

		newBoundingBox.m_min = newMin;
		newBoundingBox.m_max = newMax;

		return newBoundingBox;
		break;

	}
}

//prototype
bool load_cube_map_side(GLuint texture, GLenum side_target, const char* file_name);
void create_cube_map(
	const char* front,
	const char* back,
	const char* top,
	const char* bottom,
	const char* left,
	const char* right,
	GLuint* tex_cube);

GLuint vbo;
GLuint vao;

/* big cube. returns Vertex Array Object */
GLuint make_big_cube() {   //https://github.com/capnramses/antons_opengl_tutorials_book/blob/master/21_cube_mapping/main.cpp#L262
	float points[] = {
		-10.0f,  10.0f, -10.0f,
		-10.0f, -10.0f, -10.0f,
		10.0f, -10.0f, -10.0f,
		10.0f, -10.0f, -10.0f,
		10.0f,  10.0f, -10.0f,
		-10.0f,  10.0f, -10.0f,

		-10.0f, -10.0f,  10.0f,
		-10.0f, -10.0f, -10.0f,
		-10.0f,  10.0f, -10.0f,
		-10.0f,  10.0f, -10.0f,
		-10.0f,  10.0f,  10.0f,
		-10.0f, -10.0f,  10.0f,

		10.0f, -10.0f, -10.0f,
		10.0f, -10.0f,  10.0f,
		10.0f,  10.0f,  10.0f,
		10.0f,  10.0f,  10.0f,
		10.0f,  10.0f, -10.0f,
		10.0f, -10.0f, -10.0f,

		-10.0f, -10.0f,  10.0f,
		-10.0f,  10.0f,  10.0f,
		10.0f,  10.0f,  10.0f,
		10.0f,  10.0f,  10.0f,
		10.0f, -10.0f,  10.0f,
		-10.0f, -10.0f,  10.0f,

		-10.0f,  10.0f, -10.0f,
		10.0f,  10.0f, -10.0f,
		10.0f,  10.0f,  10.0f,
		10.0f,  10.0f,  10.0f,
		-10.0f,  10.0f,  10.0f,
		-10.0f,  10.0f, -10.0f,

		-10.0f, -10.0f, -10.0f,
		-10.0f, -10.0f,  10.0f,
		10.0f, -10.0f, -10.0f,
		10.0f, -10.0f, -10.0f,
		-10.0f, -10.0f,  10.0f,
		10.0f, -10.0f,  10.0f
	};

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(
		GL_ARRAY_BUFFER, 3 * 36 * sizeof(GLfloat), &points, GL_STATIC_DRAW);

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	return vao;
}

bool load_cube_map_side(
	GLuint texture, GLenum side_target, const char* file_name) {
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture);

	int x, y, n;
	int force_channels = 4;
	unsigned char*  image_data = stbi_load(
		file_name, &x, &y, &n, force_channels);
	if (!image_data) {
		fprintf(stderr, "ERROR: could not load %s\n", file_name);
		return false;
	}
	// non-power-of-2 dimensions check
	if ((x & (x - 1)) != 0 || (y & (y - 1)) != 0) {
		fprintf(stderr,
			"WARNING: image %s is not power-of-2 dimensions\n",
			file_name);
	}

	// copy image data into 'target' side of cube map
	glTexImage2D(
		side_target,
		0,
		GL_RGBA,
		x,
		y,
		0,
		GL_RGBA,
		GL_UNSIGNED_BYTE,
		image_data);
	free(image_data);
	return true;
}

void create_cube_map(
	const char* front,
	const char* back,
	const char* top,
	const char* bottom,
	const char* left,
	const char* right,
	GLuint* tex_cube) {
	// generate a cube-map texture to hold all the sides
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, tex_cube);

	// load each image and copy into a side of the cube-map texture
	load_cube_map_side(*tex_cube, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, front);
	load_cube_map_side(*tex_cube, GL_TEXTURE_CUBE_MAP_POSITIVE_Z, back);
	load_cube_map_side(*tex_cube, GL_TEXTURE_CUBE_MAP_POSITIVE_Y, top);
	load_cube_map_side(*tex_cube, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, bottom);
	load_cube_map_side(*tex_cube, GL_TEXTURE_CUBE_MAP_NEGATIVE_X, left);
	load_cube_map_side(*tex_cube, GL_TEXTURE_CUBE_MAP_POSITIVE_X, right);
	// format cube map texture
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

void rotateCamera(float deltaTime)
{
	View = glm::lookAt(glm::vec3(0, 1.5, -4), // Camera is at (0,1.5,-4), in World Space
		glm::vec3(0, 1.0, 0),  // and looks at x=0, y=1, z=0
		glm::vec3(0, 1, 0)); // Head is up (set to 0,1,0 to look upside-down) rotation ( auf dem Kopf usw)
	rotationCamera += 15 * deltaTime;
	View = glm::translate(View, glm::vec3(0.0f, 2.0f, 0.0f));
	View = glm::rotate(View, rotationCamera, glm::vec3(0, 1, 0));
	View = glm::translate(View, glm::vec3(0.0f, -2.0f, 0.0f));
}

void reset() {
	paused = false;
	highscore = 0;
	rotationCamera = 0.0f;
	player.velocity = 50;
	player.lifes = player.maxLifes;
	player.x = 0;

	obstaclesOnField.clear();
}